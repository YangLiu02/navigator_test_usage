### 1.现有的test

#### 1.1系统功能性测试

###### 1.navigator_ros/launch/tool_launch

`tool_launch/evaluation_data_collector.launch    tool_trajectorizer_evaluation_2d.cpp`

`tool_launch/tool_evaluation_plot.launch    evaluation_process.py`

`tool_launch/tool_fake_elevator.launch   fake_elevator.cpp`　＃电梯任务脚本

`tool_fake_sick_tim_310.launch  tool_fake_sick_310.cpp`

`tool_tasksimplex.launch  tool_task_simplex_sender_2d.cpp`

`tool_point_controller_evaluation_2d.launch  tool_point_controller_evaluation_2d.cpp`

`tool_static_occupancy_map.launch  tool_static_occupancy_map.cpp`

`tool_test_custom_pid_controller.launch  tool_custom_pid_controller_2d.cpp`

`tool_launch/tool_trajectorizer_2d.launch  tool_trajectorizer_2d.cpp`

`tool_trajectory_data_collect.launch    tool_trajectory_data_collect.cpp` 

`tool_trajectory_plot.launch  plot.py`

###### 2.navigator/test

test_planner_on_lattice.cpp

###### 3.navigator/src/test

...

###### 4.navigator/src/unittest

planner_test.cpp

#### 1.2函数功能测试



### 2.要新增的test

主要是node_local_plan